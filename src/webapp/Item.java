package webapp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@WebServlet(urlPatterns = "/item")
public class Item extends HttpServlet {

    DataSource pool;

    @Override
    public void init() throws ServletException {
        super.init();
        pool =(DataSource) getServletContext().getAttribute("dbPool");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String itemCode = request.getParameter("itemCode");
        String itemName = request.getParameter("itemName");
        String itemPrice=request.getParameter("unitPrice");
        String itemQty=request.getParameter("QtyOnHand");
        Connection connection=null;
        PreparedStatement statement=null;


        try {
            connection= pool.getConnection();
            String sql="INSERT INTO Item VALUES(?,?,?,?)";
            statement = connection.prepareStatement(sql);
            statement.setObject(1,itemCode);
            statement.setObject(2,itemName);
            statement.setObject(3,itemPrice);
            statement.setObject(4,itemQty);

            Boolean isSubmited=(statement.executeUpdate() >0);
            if(statement.executeUpdate()>0){
                request.getRequestDispatcher("/item").forward(request,response);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            if(statement !=null){
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(connection !=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
