package webapp.db;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class DBConnection {
    public static DataSource getConnectionPool(){
        DataSource pool=null;
        InitialContext context = null;
        try {
             context= new InitialContext();
             pool=(DataSource)context.lookup("java:comp/env/mysample");
        } catch (NamingException e) {
            e.printStackTrace();
        }

        return pool;
    }
}
